﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DemoInterview
{
    public class Algorithm
    {
        private readonly string ticker;

        public Algorithm(string ticker)
        {
            this.ticker = ticker;
        }

        public event EventHandler<TradeEvent> TradeEvent;
        
        private void DoTrade(object sender, Trade trade)
        {
            var eventArgs = new TradeEvent(trade);
            this.TradeEvent?.Invoke(sender, eventArgs);
        }

        public void Run()
        {
            var rand = new Random();
            while (true)
            {
                var randomVal = rand.NextDouble();
                var price = (randomVal * 40) + 80; // random price between 80-120
                var trade = new Trade()
                {
                    Ticker = ticker,
                    Side = (price > 100) ? "BUY" : "SELL",
                    Price = price,
                    Quantity = 10
                };

                randomVal = rand.NextDouble();
                Thread.Sleep(Convert.ToInt32(randomVal * 1000)); // random sleep

                DoTrade(this, trade);
            }
        }
    }
}
