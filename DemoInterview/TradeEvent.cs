﻿using System;

namespace DemoInterview
{
    public class TradeEvent : EventArgs
    {
        public TradeEvent(Trade trade)
        {
            this.Trade = trade;
        }

        public Trade Trade { get; set; }
    }
}
