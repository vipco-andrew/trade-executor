﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterview
{
    public class TradeExecutor
    {
        private readonly ITradeRecorder tradeRecorder;
        private readonly Random rand = new Random();

        public TradeExecutor(ITradeRecorder tradeRecorder)
        {
            this.tradeRecorder = tradeRecorder;
        }

        public async Task SendTradeForProcessing(int id, Trade trade)
        {
            // Random delay
            await Task.Delay(1000);

            // 1% probability of failure
            var randomVal = rand.NextDouble();
            var success = randomVal > 0.01;
            if (success)
            {
                var processed = new ProcessedTrade(id, DateTime.Now, trade);
                await tradeRecorder.RecordProcessedTrade(processed);
                return;
            }
            else
            {
                throw new TimeoutException();
            }
        }

    }
}
