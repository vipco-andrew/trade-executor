﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterview
{
    public interface ITradeRecorder
    {
        Task RecordProcessedTrade(ProcessedTrade processed);
    }
}
