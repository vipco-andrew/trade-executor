﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterview
{
    public class ProcessedTrade
    {
        public ProcessedTrade(int id, DateTime timestamp, Trade trade)
        {
            this.Id = id;
            this.Timestamp = timestamp;
            this.Trade = trade;
        }

        public int Id { get; private set; }
        public DateTime Timestamp { get; private set; }
        public Trade Trade { get; private set; }
    }
}
