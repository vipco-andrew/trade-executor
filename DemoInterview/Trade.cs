﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInterview
{
    public class Trade
    {
        public string Ticker { get; set; }
        public string Side { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }
}
