﻿using System;
using System.Threading.Tasks;

namespace DemoInterview
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var algos = new[]
            {
                new Algorithm("AAPL"),
                new Algorithm("MSFT"),
                new Algorithm("AMZN")
            };

            StartAlgos(algos);
        }

        static void StartAlgos(Algorithm[] algos)
        {
            /*
             * Tasks:
             *  - Start the algorithms concurrently
             *  - Listen for generated trades
             *  - Execute the trades via the 'TradeExecutor' class
             *  - Implement 'ITradeRecorder'
             * 
             * Requirements:
             * - Handle backpressure from TradeExecutor
             * - Uniquely identify each trade using an Int id
             * - Ensure all processed trades are recorded in the same 'ITradeRecorder' instance
             * 
             */


        }

    }
}
